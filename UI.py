#!/usr/bin/env python3
import tkinter as tk
from tkinter import *
from tkinter import HORIZONTAL, LEFT, ttk, Canvas
from tkinter import filedialog
import ttkbootstrap as tkboot
from ttkbootstrap import ttk as TTK
from ttkbootstrap import font as tkfont
from tkinter import HORIZONTAL
from ttkbootstrap.constants import *
from idlelib.percolator import Percolator
from idlelib.colorizer import ColorDelegator
from idlelib.tooltip import Hovertip
from tkinter.filedialog import asksaveasfilename
from main import * 

# filepath = 

#Container class of entire application
class App(tk.Tk):
    #Logic for generating a tab
    def gen_tab(self):
        self.tab_counter += 1
        tab = tk.Frame(self.container, width=1920, height=1080)
        tab.grid_columnconfigure(0, weight=1)
        tab.grid_rowconfigure(0, weight=1)
        self.container.add(tab, text=f'Tab {self.tab_counter}')
        tab.grid_rowconfigure(0, weight=1)
        tab.grid_columnconfigure(0, weight=1)
        loop = 0 
        data = ["StartPage", "musicPlayerPage"]
        for F in (StartPage, musicPlayerPage):
            current_frame = F(parent=tab, controller=self, master=tab)
            current_frame.grid_columnconfigure(0, weight=1)
            current_frame.grid_rowconfigure(0, weight=1)
        
            if loop == 0:
                self.frames.append({})
            self.frames[self.tab_counter - 1][data[loop]] = current_frame
            current_frame.grid(row=0, column=0)
            loop += 1

     #Logic for deleting a tab 
    def destroy_tab(self):
        for F in self.container.winfo_children():
            if str(F)==self.container.select():
                F.destroy()
                return

    def __init__(self, *args, **kwargs):
        self.tab_counter = 0
        self.tab_list = []
        self.frames = []

        tk.Tk.__init__(self, *args, **kwargs)
        self.title('MANGO')
        self.geometry('1920x1080')
        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(0, weight=1)
        tkboot.Style('darkly')

   
        self.title_font = tkfont.Font(family='Century Schoolbook', size=18, weight="bold", slant="italic")
        self.container = ttk.Notebook(self)

        self.container.grid(row=0, column=0)
        self.gen_tab()
        self.container.grid_rowconfigure(0, weight=1)
        self.container.grid_columnconfigure(0, weight=1)

        #Button for generating a notebook tab
        self.new_tab = tkboot.Button(
           self,
           command=self.gen_tab,
           width="30",
           text="New Smoothie Blender Tab",
           bootstyle=SECONDARY
        )
        self.new_tab.grid(row=0, column=0, padx=(0,0), pady=(70,400), sticky='w')
        self.new_tabTip = Hovertip(self.new_tab, 'Create a new Smoothie Blender tab!')
        
        #Delete button for a notebook tab
        self.delete_tab = tkboot.Button(
            self,
            command=self.destroy_tab,
            width="30",
            text="Unplug Smoothie Blender Tab",
            bootstyle=SECONDARY
        )
        self.delete_tab.grid(row=0, column=0, padx=(0,0), pady=(0,400), sticky='w')
        self.delete_tabTip = Hovertip(self.delete_tab, 'Unplug the Smoothie Blender Tab!')
        

    def show_frame(self, page_name):
        frame = self.frames[page_name]
        frame.tkraise()

#If we want a main home page this would be it, we dont need this
class StartPage(tk.Frame):
    def __init__(self, parent, controller, master):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        #to be continued

#If we wanted a branch page off of the main home page this would be it

class musicPlayerPage(tk.Frame):
     def __init__(self, parent, controller, master):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        #function for generating a sound effect
        def gen_effect(event=None):
            if event == None:
                effect = ''
            else:
                effect = event.widget.get()
            # Reset Button Mechanic with Effect Dropbox
            # if effect == 'Default / Reset':
            change_effect(effect)#, filepath)
            # else:
            #     change_effect(effect)

        #Returns user set volume
        def on_change(enter_vol):
            if(enter_vol.widget.get()):
                volume = int(enter_vol.widget.get())
                print("The UI has this volume: ", volume)
                genNotes.volume = volume
                # print("Main.py has this volume:")
            else:
                volume = 3
            #to be continued...

        #Here is where we will declare/ design all UI widgets such as buttons:

        #Function to open a file, called within, that also displays the file in the UI
        def openFile():
           
            filepath = filedialog.askopenfilename()
            file = open(filepath, 'r')
            print("File is: ", filepath)
            if not filepath.endswith(".py"):
                if filepath.endswith(".wav"):
                    genNotes.wev = filepath
                else:
                    warning = Text(self.right, font= ('{Comic Sans MS} 15'))
                    warning.insert(tk.END, "File must be a Python file.")
                    warning.config(state=DISABLED)
                    warning.grid(row=0, padx=(0, 0), pady=(0,0), sticky='e')
                    print("File must be a Python file.")
                return
            main(filepath)
            
        
            file_contents = file.read()
            # print(file_contents) #Prints the file to terminal, maybe we can print a preview of it in the GUI
           

            #This shows the Python file in the UI using a 'Label' widget:
            #python_file_plc.grid_forget()
            python_file = Text(self.right, font= ('{Comic Sans MS} 15'))
            python_file.insert(tk.END, file_contents)
            python_file.config(state=DISABLED)
            python_file.grid(row=0, padx=(0, 0), pady=(0,0), sticky='e')
            #Syntax highlighting:
            cdg = ColorDelegator(); 
            cdg.tagdefs['COMMENT'] = {'foreground': 'lightpink', 'background': '#2F2F2F'}
            cdg.tagdefs['KEYWORD'] = {'foreground': 'lightgreen', 'background': '#2F2F2F'}
            cdg.tagdefs['BUILTIN'] = {'foreground': '#7F7F00', 'background': '#2F2F2F'}
            cdg.tagdefs['STRING'] = {'foreground': 'cyan', 'background': '#2F2F2F'}
            cdg.tagdefs['DEFINITION'] = {'foreground': 'lightpink', 'background': '#2F2F2F'}
            Percolator(python_file).insertfilter(cdg)
  
            #Scrollbar for window
            scroll_bar = tk.Scrollbar(self.right, orient=VERTICAL) #orient='horizontal')
            
            python_file['yscrollcommand'] = scroll_bar.set
            scroll_bar['command']=python_file.yview
            
            file.close()

        #Here is where we will declare/ design all UI widgets such as buttons
        def widgets(frame):
                self.tab = tk.Frame(frame)
                self.tab.pack()
                self.left = tk.Frame(self.tab)
                self.left.grid(column=0, row=0)
                self.right = tk.Frame(self.tab)
                self.right.grid(row=0, column=1)

                #Smoothie_Blender (notebook_label) widget  
                self.notebook_label = tk.Label(self.left, font="{Comic Sans MS} 12")
                self.notebook_label.grid(row=0, padx=(10,0), pady=(10, 30), sticky='w')
    
                #Placeholder for python file
                python_file_plc = Text(self.right, font= ('{Comic Sans MS} 15'))
                python_file_plc.insert(tk.END, "Upload a Python file to put it inside the Blender!")
                python_file_plc.config(state=DISABLED)
                python_file_plc.grid(row=0, padx=(0, 0), pady=(0,0), sticky='e')

                #Button for importing a Python file
                self.upload_file = tkboot.Button(
                self.left, 
                #command=function_here, function we want to call when this button is clicked
                #added openFile() function to do so, not sure if you want to keep it in the same class
                width="30", 
                text="Upload Python file (.py)", 
                bootstyle=SUCCESS,
                command=openFile
                )
                #This puts the button in the UI
                self.upload_file.grid(row=0, padx=(70,0), pady=(20,0), sticky='w')
                self.upload_file_tip = Hovertip(self.upload_file, 'Upload a Python file')

                #VOLUME ENTRY BOX
                self.vol_label = tk.Label(self.left, font="{Century Schoolbook} 10", text="Volume:")
                self.vol_label.grid(row=2, padx=(50, 0), pady=(20,0), sticky='w')
                self.enter_vol = tk.Entry(self.left, font="{Century Schoolbook} 10", width="8")
                self.enter_vol.grid(row=2, padx=(135, 0), pady=(20,0), sticky='w')
                self.enter_vol.bind("<Return>", on_change)
                
                
                #PLAY BUTTON
                #Label for play button
                self.play_label = tk.Label(self.left, font="{Century Schoolbook} 8", text="Play Smoothie")
                self.play_label.grid(row=4, padx=(40, 0), pady=(30,0), sticky='w')
                #Start button
                self.play_btn = tkboot.Button(
                self.left, 
                command=play_wav,
                width="10", 
                text="Start", 
                bootstyle=SECONDARY
                )

                self.play_btn.grid(row=5, padx=(50,0), pady=(5,0), sticky='w')
                self.play_btn_tip = Hovertip(self.play_btn, 'Play Smoothie')

                #STOP BUTTON
                #Label for stop button
                self.stop_label = tk.Label(self.left, font="{Century Schoolbook} 8", text="Stop Smoothie")
                self.stop_label.grid(row=4, padx=(210, 20), pady=(30,0), sticky='w')
                #Stop button
                self.stop_btn = tkboot.Button(
                self.left, 
                command=stop_wav,
                width="10", 
                text="Stop", 
                bootstyle=SECONDARY
                )
                self.stop_btn.grid(row=5, padx=(220,20), pady=(5,0), sticky='w')
                self.stop_btn_tip = Hovertip(self.stop_btn, 'Stop Smoothie')

                #EFFECTS DROPDOWN
                #Sound effects dropdown widget
                self.dropdown_effects = TTK.Combobox(self.left, font="{Century Schoolbook} 10")
                self.dropdown_effects.set('Select Flavors (Effects)...')
                self.dropdown_effects['state'] = 'readonly'
                self.dropdown_effects['values'] = (['Default / Reset', 'AutoWah', 'Crunchy', 'Distortion', 'Ring', 'Robot', 'Smooth', 'Tremolo', 'Vibrato'])
                self.dropdown_effects.bind('<<ComboboxSelected>>', gen_effect) #the 'gen_effect' function is called when a selection from this dropdown is made
                self.dropdown_effects.grid(row=6, padx=(70,0), pady=(50, 0), sticky='w')
                self.effects_tip = Hovertip(self.dropdown_effects, 'Select Flavors (Effects)!')

                #To be continued...
                
                return self.tab

        frame = ttk.Notebook(self)
        frame.pack(fill='both', pady=10, expand=True)
        tab1 = widgets(frame)
        frame.add(tab1, text = "Current Smoothie Blender Tab") 


def start_ui():
    app = App()
    app.mainloop()

if __name__ == "__main__":
    start_ui()
#!/usr/bin/env python3
# CS 410P [Music, Sound, & Computers], Spring 2022 - Musical Analysis Note Generation Operator (M.A.N.G.O.) - Course Project
# Team Members: Adriana Swantz, Alissa Friel, Naya Mairena, and Dan Jang

# Purpose: 
#  To create a musical analysis note generation operator (M.A.N.G.O.),
#  that can be used to generate musical analysis notes with Python (.py) code-source files,
#  based on key, topological syntax-processing methodologies,
#  with customized differentiation for each specific groups of syntax & programming variables.

### 1. Parsing Libraries
import ast
# from parse import *

### 2. Musical Libraries
import wave
from scipy import signal
import sounddevice as sd
import soundfile as sf

### 3. Mathematicaly & Miscellaneous Libraries
import sys
import shutil
from platform import python_version
import numpy as np
from random import randint

# call in the code file and parse the file, by reading the function types , variable types, save into
# an array maybe

## Initialization: GLOBAL VARIABLES
sample_rate = 48000
# filepathz = None

# Part A: The Parsing Code
class genNotes():
  libs = []
  vars = []
  volume = 10

class AssignmentVisitor(ast.NodeVisitor):

  # this function parses the function names, and the variables within said function. It is put into an array which is appended to another array at the end.
  def visit_FunctionDef(self, node):
    x = []
    for defi in ast.walk(node):
      if isinstance(defi, ast.FunctionDef):
        # print("this is a function: ", defi.name)
        x.append(defi.name)
        for target in ast.walk(defi):
          if isinstance(target, ast.Assign):
            for tar in target.targets:
              if isinstance(tar, ast.Name):
                # print("and this is a variable in ", defi.name, ": ", tar.id)
                x.append(tar.id)
        genNotes.vars.append(x)
        x = []
 
  # These two parse out library names, the first gets regular imports and the second catches import from statements
  def visit_Import(self, node):      
    for alias in ast.walk(node):
      if isinstance(alias, ast.Import):
        for name in alias.names:
          # print(name.name, end=' ')
          genNotes.libs.append(name.name)

  def visit_ImportFrom(self, node):
    for module in ast.walk(node):
      if isinstance(module, ast.ImportFrom):
        # print(module.module, end=' ')
        genNotes.libs.append(module.module)
  
# This function runs the parsing fucntions to start the breakdown of the code in order to make lovely (or horrible) music.
def parseMe(selection):
  with open(selection, "rb") as source:
    tree = ast.parse(source.read())
  # with open(sys.argv[1], "rb") as source:
  #  tree = ast.parse(source.read())
  #print(ast.dump(tree))
  AssignmentVisitor().visit(tree)

def tremolo():
  pass

# Reference & Credit to: https://stackoverflow.com/questions/28185219/generating-vibrato-sine-wave
def vibrato():
  filename = 'smoothieTestUI.wav'
  data, fs = sf.read(filename, dtype='float32')
  
  depth = 1.0 / fs
  length = len(data)
  
  for idx in range(length):
    x = data[idx]
    y = 440.0 - 10 * np.sin(2 * np.pi * x * 1.69) # 1.69 = oscillates at 1.69 Hz
    data[idx] = y
    
  sf.write(filename, data, fs)
  
  # pass

def distortion():
  filename = 'smoothieTestUI.wav'
  data, fs = sf.read(filename, dtype='float32')

  threshold = 0.5
  npsignal = len(data)

  for i in range(npsignal):
    x = data[i]
    if x > threshold:
      y = threshold
    elif x < -threshold:
      y = -threshold
    else:
      y = x
    data[i] = y
  sf.write(filename, data, fs)

def distortion_smooth():
  filename = 'smoothieTestUI.wav'
  data, fs = sf.read(filename, dtype='float32')

  threshold = 0.5
  smooth = 2

  for i in range(len(data)):
    x = data[i]
    y = 2 * (1 / (1 + np.exp(-smooth * (0.5 * x + 0.5)))) - 1
    data[i] = y
  sf.write(filename, data, fs)

def distortion_crunch():
  filename = 'smoothieTestUI.wav'
  data, fs = sf.read(filename, dtype='float32')

  threshold = 0.5
  crunch = 1

  for i in range(len(data)):
    x = data[i]
    y = 0.25 * int(2 ** crunch * x) / 2 ** crunch
    data[i] = y
  sf.write(filename, data, fs)

def robot():
  filename = 'smoothieTestUI.wav'
  data, fs = sf.read(filename, dtype='float32')

  threshold = 0.5
  crunch = 0.5

  for i in range(len(data)):
    x = data[i]
    y = 0.25 * int(2 ** crunch * x) / 2 ** crunch
    data[i] = y
  sf.write(filename, data, fs)


def ring():
  filename = 'smoothieTestUI.wav'
  data, fs = sf.read(filename, dtype='float32')
  rate = 0.5
  blend = 0.5
  t = 0
  for i in range(len(data)):
    ring_fac = np.sin(t)
    t += (rate * 0.02)

    if t > 6.28318531:
      t -= 6.28318531
    data[i] = ((1.0 - blend) * data[i]) + blend * ring_fac * data[i]
  sf.write(filename, data, fs)


def autowah():
  filename = 'smoothieTestUI.wav'
  sound = wave.open(filename, 'rb')
  info = sound.getparams()
  sound.close()
  data, fs = sf.read(filename, dtype='float32')

  w_rate = 5
  depth = 0.02
  height = 0.9
  lap = 4

  wah_rate = lap / w_rate
  rate = info.framerate

  blocksize = rate // 100
  window = signal.windows.hann(blocksize)
  ncontour = int(wah_rate * rate / blocksize)
  scale = height - depth
  cspace = np.linspace(0, np.pi, ncontour)
  contour = scale * np.log2(2 - np.sin(cspace)) + depth
  fws = [ signal.firwin(128, c, window=('kaiser', 0.5))
          for c in contour ]

  icontour = 0
  wahed = np.zeros(info.nframes)
  start = 0
  while start < info.nframes:
      end = min(start + blocksize, info.nframes)
      block = data[start:end]
      icontour = (icontour + 1) % ncontour
      block = signal.convolve(block, fws[icontour], mode='same')
      invlap = 1.0 / lap
      for i in range(start, end):
          wahed[i] += invlap * block[i - start] * window[i - start]
      start += blocksize // lap


  sf.write(filename, wahed, fs)

#This function plays the WAV file when the Start button is pressed in the UI.
def play_wav():
  filename = 'smoothieTestUI.wav'
  data, fs = sf.read(filename, dtype='float32')
  sd.play(data, fs)

# Called from UI.py to stop the music when the stop button is pressed.  
def stop_wav():
  sd.stop()

# Called from UI.py via its gen_effect, to main.py's change_effect() to restore original wav!
def reset():
  shutil.copy2('smoothieOriginal.wav', 'smoothieTestUI.wav')
  print("\nSuccessfully reset to the default flavor smoothie.")

#Function is called from UI.py in gen_sound function to send the effect choice chosen by the user utilizing the dropdown menu.
def change_effect(option):#, filepathz):
  if option == 'Default / Reset':
    # print(filepathz)
    # if filepathz != None:
    #   main(filepathz)
    #   print("\nSuccessfully reset to the default flavor smoothie.")
    # elif filepathz == None:
    #   print('\nuh oh, looks like the blender is currently empty')
    #   print('please add some ingredients before attempting to make a reset smoothie!')
    reset()
    #pass
  elif option == 'AutoWah':
    autowah()
  elif option == 'Crunchy':
    distortion_crunch()
  elif option == 'Distortion':
    distortion()
  elif option == 'Ring':
    ring()
  elif option == 'Robot':
    robot()
  elif option == 'Smooth':
    distortion_smooth()
  elif option == 'Vibrato':
    print("\nOh noes! Our Vibrato flavor ingredients are currently out of stock (WIP)!")
    # print("vibrato test")
    # commented out below until troubleshooted lol
    # vibrato()#width, frequency, file, filename)
  elif option == 'Tremolo':
    print("\nOh noes! Our Tremolo flavor ingredients are currently out of stock (WIP)!")
    #tremolo(DataOption, ExportType, frequency, depth, gain, file, filename)

def notes(libs):
  roots = []
  for i in range(len(libs)):
    x = (len(libs[i]))%7
    nroots = []
    for j in range(len(libs[i])):
      # length of library, determines fraction of midi notes starting a 20. So 1 would be the range of the midi notes 30-40  
      y = 20 + x*10 + (ord(libs[i][j])%10)
      
      nroots.append(y)
    roots.append(nroots)  
  return roots

def chord(freq, volume, beat, b_samples, ramp_frac):
  n = np.linspace(0, beat, b_samples, False)
  chords = freq[randint(0, len(freq) -1)]
  sine = [np.sin(2 * np.pi * f * n) for f in chords]
  data = [volume * sin * (32767) / np.max(np.abs(sin)) for sin in sine]
  # ramping of the notes
  for i in range(len(data[0])):
    for d in data:
      if i < ramp_frac:
        d[i] *= (i/ramp_frac)
      if i> b_samples - ramp_frac:
        d[i] *= ((b_samples - i)/ramp_frac)
  post = [d.astype(np.int16) for d in data]
  
  # make chord
  for i in range(len(post[0])):
    post[0][i] = post[0][i]/3 + post[1][i]/3 + post[2][i]/3
  return post[0]

def singleNote(freq, volume, beat, b_samples, ramp_frac):
  
  n = np.linspace(0, beat, b_samples, False)
  sine = [np.sin(2 * np.pi * f * n) for f in freq]
  data = [volume * sin * (32767) / np.max(np.abs(sin)) for sin in sine]
  
  for i in range(len(data[0])):
      for d in data:
        if i < ramp_frac:
          d[i] *= (i/ramp_frac)
        if i> b_samples - ramp_frac:
          d[i] *= ((b_samples - i)/ramp_frac)
  post = [d.astype(np.int16) for d in data]
  return post

# creating a list of frequencies based on the range of notes chosen
def freqi(scale, notes, measure, bpm, sound):
  beat = 60/bpm
  b_samples = int(beat*sample_rate)
 
  volume = 10**((-6*(10-genNotes.volume))/20)
  # print(volume)
  ramp = 0.3
  ramp_frac = ramp*b_samples
  moreNotes = []
  for i in range(len(notes)):
    n = []
    for j in range(len(scale)):
      # steping through each note in the list and making another list of notes based on the scales chosen
      n.append(440*(2**((notes[i]+scale[j]-69)/12)))
    
    # the new set of notes to be used this round
    moreNotes.append(n)
  
  # checks the type of scale and write the notes based on this, for the length of the measure
  if len(scale) == 3:
    for i in range(measure):
      
      post = chord(moreNotes, volume, beat, b_samples, ramp_frac)
      sound.writeframes(post)
    
  elif len(scale) == 7:
    leng = len(moreNotes) -1
    x = randint(0, leng)
    chords = moreNotes[x]
    post = singleNote(chords, volume, beat, b_samples, ramp_frac)
    for i in range(measure):
      sound.writeframes(post[randint(0,len(chords)-1)])

  else:
    assert False
    # for i in range(measure):

### https://github.com/Quefumas/gensound/wiki/Effects

def music(notes, vfs, bpm):
  # number of measures determined by the number of functions
  sample_rate = 48000
  sound = wave.open('smoothieTestUI.wav', 'wb')
  sound.setnchannels(1)
  sound.setframerate(sample_rate)
  sound.setsampwidth(2)
  # number of measures determined by number of functions
  for i in range(len(vfs)):
    moreNotes = []
    # measure size determined by number of variables in function (min 1)
    measure = len(vfs[i][0])
    # choose range of notes to use
    rng = measure % len(notes)
    # choose what type of scale to use
    scalefac = len(vfs[i]) % 4
    if scalefac == 0:
      #majorscale
      scale = [0, 2, 4, 5, 7, 9, 11]
    elif scalefac == 1:
      #minorscale
      scale = [0, 2, 3, 5, 7, 9, 10]
    elif scalefac == 2:
      #majorchord 
      scale = [0,4,7]
    elif scalefac == 3:
      #minorchord 
      scale= [0,3,7]
    else:
      assert False

    # creating a list of frequencies based on the range of notes chosen
    freqi(scale, notes[rng], measure, bpm, sound)
  
  sound.close()
  shutil.copy2('smoothieTestUI.wav', 'smoothieOriginal.wav') #Making a copy for reset()
  
"""
  Used to test that the lists are coming in right
  for i in range(len(vfs)):
    for j in range(len(vfs[i])):
      print(vfs[i][j], end=" ")
    print()
  for i in range(len(libs)):
    print(libs[i])
"""
def main(selection):
  if selection != 0:
    # I.) Parsing Functions
    # if selection != None:
    #  filepathz = selection # For Reset Button
    parseMe(selection)
    note = notes(genNotes.libs)
    music(note, genNotes.vars, 240)
    
# Main function to start the program
if __name__ == "__main__":
  
  '''
  # Adding new code that will basically run UI.py instead of main.py (this file)
  # If main.py was just ran directly
  '''
  
  if sys.version_info.major >= 3:
    print("Welcome to the M.A.N.G.O. Software!")
    print("\nLoading the M.A.N.G.O. Blender GUI...")
    # main(1)
    
    exec(open("UI.py").read())
    
    print("Thank you for using M.A.N.G.O.!\n\nHave a lovely day or night, hehe xoxo\n")
  
  elif sys.version_info.major <= 2:
    # This will give you an error probably because you are probably
    # using Python 3 or higher - but a necessary OCD annoyance perhaps, so
    # those with Python 2 or lower will be possibly able to use this code.
    
    # EDIT: Actually, gonna replace it with statement stating that they
    # should use Python 3+ anyways because things may break if not Python 3 or higher.
    
    print("This program is supported with Python 3 or higher.\n")
    print("Looks like you are currently running: Python " + python_version() + "!\n")
    print("M.A.N.G.O. will attempt to launch anyways, momentarily.")
    print("You may also choose to try running the program anyways by directly running the UI.py file!\n")
    
    # EDIT 2: Nah, will include execfile() / have M.A.N.G.O. run anyways cause why not lol
    
    print("Welcome to the M.A.N.G.O. Software!")
    print("\nLoading the M.A.N.G.O. Blender GUI...")
    
    execfile("UI.py") # Ignore "not defined" error, that just means you aren't runnin' Python 2 right now lmao
    
    print("Thank you for using M.A.N.G.O.!\n\nHave a lovely day or night, hehe xoxo\n")
    
    # main(0)
    
  # Commenting this out as I am assuming the below was for debugging purposes
  
  # if len(sys.argv) > 1:
  #   main(1)
  # else:
  #   main(0)
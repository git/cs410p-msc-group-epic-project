# CS 410P: Music, Sound, & Computers (Spring 2022)

## M.A.N.G.O. Course Project

This is the readme file for the **M.A.N.G.O. (Musical Analysis Note Generation Operator) Course Project** for Music, Sound, & Computers by Professor Dr. Bart Massey.

## Team Members -

* Adriana Swantz
* Alissa Friel
* Naya Mairena
* Dan Jang (commits as "User name")

### Project Description -

[Project Introduction & Demonstration Video](https://youtu.be/zqzky2YwlPs)

An interpreter of Python programming implementation files that uses specific aspects of the programming language, specific to Python, such as functions, variables, and types of code to generate quasi-harmonic music based on an intelligent recognition system.

For example, the length of the name of any given type of Python code may have effects on pitch and harmonics. Moreover, if there is a specifically higher number of specific types of variables over another, e.g. more variables than there are functions, and etc.

## How To Install Dependencies & Run Program?

For Python 3 or higher -

```py
pip3 install -r requirements.txt
```

For Python 2 or below -

```py
pip install -r requirements.txt
```

## How to Use -

**Before Starting**: Make sure you have installed requirements.txt with pip(3) as above.

1. You **may choose** to run **main.py normally** or directly with UI.py.
2. Enter **volume** - from *1 to 10* - and then hit the **enter** button.
3. **Upload Python file** (**.py**) of your choosing. If you are running M.A.N.G.O on a Mac or Linux system, make sure your chosen Python file is in the same directory as M.A.N.G.O. (idk why no work)
4. *Please wait momentarily* while the program '**blends**' (**processes the user-inputted, Python cod**e) your code into a '**musical smoothie'** (with **.wav** format). This may be *slightly long* if you choose a big file.
5. Hit the **play** button, enjoy your smoothie!
6. Not enjoying the smoothie? Hit the stop button.
7. Want to ***make it hit different***? Choose what **effects** you'd like to be **mixed in** to your smoothie.
8. Please feel free to keep going. Blend more effects to the mix, and make a monster smoothie. Or upload the same or a different file again to start a *new* '**blend**'.

### Rundown of the M.A.N.G.O Design & Functionality -

1. From the **M.A.N.G.O.** **GUI**, the *user chooses the* P*ython file they want processed*, which will be verified to ascertain that it is indeed, a Python file. If verified, the main function is called from main.py.
2. In **main.py**, the **parseMe()** function runs the file through our **parser functions**, which stores the **functions** and **variables** from the user-specified code, in **separate** and **specific subarrays**, that will be ***combined*** to create a **2-Dimensional arrays of strings**, based on the parsed user-code'. Additionally, M.A.N.G.O. creates a list of *imported library names* as strings.
3. The notes() function will determine the range of MIDI notes - *staying within 20-90 keys of human hearing* - based on the number of letters of each respective library names & ASCII values.
4. Continuing with the **start of music generation**, the **music()** function is called. The **music() function** will determine, *number of measures*, *number of beats* in said measures, *scale* to be applied to notes, & then *randomly play notes from that selection* - based on the **arrays of parsed information** from prior-called, parsing functions as aforementioned.
5. Users can choose to 'mix-in' an **effect** or **multiple effects**, to the 'smoothie' (**generated .wav file**) if they wish, such as *tremolo*, *ring*, *autowah*, and *distortion*. Choosing '**Default / Reset**' will ***revert*** the 'mixed' smoothie to its 'default' flavor.

## Project Specification -

The **Language**

* Using Python, to implement the program.
* User-inputted code will be also in Python format.

The **Parsing, Musical, & Additional / Miscellaneous Libraries**

* ast
* sys
* wave
* scipy
* sounddevice
* soundfile
* numpy
* random
* tkinker
* ttkbootstrap
* idlelib
* shutil

### Sources & Reference Material -

[https://wiki.analog.com/resources/tools-software/sharc-audio-module/baremetal/tremelo-effect-tutorial](https://wiki.analog.com/resources/tools-software/sharc-audio-module/baremetal/tremelo-effect-tutorial)

[https://wiki.analog.com/resources/tools-software/sharc-audio-module/baremetal/ring-modulator-effect-tutorial](https://wiki.analog.com/resources/tools-software/sharc-audio-module/baremetal/ring-modulator-effect-tutorial)

[https://github.com/pdx-cs-sound/effects](https://github.com/pdx-cs-sound/effects)
